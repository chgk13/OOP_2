class MetaEnum(type):
    def __new__(metacls, cls, bases, namespace):
        slot_dict = {}
        for name, value in namespace.items():
            if name == name.strip('__'):
                slot_dict[name] = value
        namespace['_slot_dict'] = slot_dict

        myenum = super().__new__(metacls, cls, bases, namespace)
        enum_dict = {}
        for name, value in slot_dict.items():
            tmp_enum = myenum(value)
            enum_dict[name] = tmp_enum
            setattr(myenum, name, tmp_enum)

        setattr(myenum, '_enum_dict', enum_dict)
        return myenum

    def __iter__(cls):
        return (getattr(cls, a) for a in cls._enum_dict)

    def __getitem__(cls, name):
        return getattr(cls, name)


class MyEnum(metaclass=MetaEnum):
    def __new__(cls, value):
        if value not in cls._slot_dict.values():
            raise ValueError(f'{value} is not a valid {cls.__name__}')

        for name, obj in cls._enum_dict.items():
            if value == getattr(obj, 'value'):
                return obj

        for name, val in cls._slot_dict.items():
            if val == value:
                tmp_enum = super().__new__(cls)
                setattr(tmp_enum, 'name', name)
                setattr(tmp_enum, 'value', value)
                return tmp_enum

    def __str__(self):
        return f'{self.__class__.__name__}.{self.name}: {self.value}'


class Direction(MyEnum):
    north = 0
    east = 90
    south = 180
    west = 270


print(Direction.north, Direction.north.name, Direction.north.value, sep='; ')

for d in Direction:
    print(d)

print(id(Direction.north))
print(id(Direction(0)))

Direction(30)
