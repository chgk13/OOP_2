class OurDescriptor:
    def __init__(self, var_name, border=0):
        self.var_name = var_name
        self.border = border

    def __get__(self, obj, objtype=None):
        return getattr(obj, self.var_name, objtype)

    def __set__(self, obj, value):
        if value <= self.border:
            raise ValueError(
                f'{self.var_name} must be greater than {self.border}.')
        elif value > 0:
            setattr(obj, self.var_name, value)
        else:
            raise Exception("You shouldn't be here.")


class BePositive:
    some_value = OurDescriptor('some')
    another_value = OurDescriptor('another')
    third_value = 3
    more_than_ten = OurDescriptor('greater_then_ten', 10)

    def this_big(self):
        self.big = OurDescriptor('big')
        self.big = -100
        self.__class__.third_value = OurDescriptor('third')


def test():
    # initial tests:
    instance = BePositive()
    instance.some_value = 1
    print('some:', instance.some_value)
    try:
        instance.another_value = -2
    except Exception as ex:
        print(f"We've got an exception: {ex}")
    instance.another_value = 11
    print('another:', instance.another_value, end='\n\n')

    # testing descriptor inside method
    instance.third_value = -2
    print('third:', instance.third_value)
    instance.this_big()
    try:
        instance.third_value = -1
    except Exception as ex:
        print(f"We've got an exception {ex}")
    instance.third_value = 10
    print(instance.third_value)
    print(instance.__dict__, end='\n\n')

    # testing descriptor with another instance of this class
    second_instance = BePositive()
    print(second_instance.__dict__)
    second_instance.some_value = 3
    try:
        second_instance.some_value = -3
    except Exception as ex:
        print(f"We've got an exception: {ex}")
    print('first_some', instance.some_value)
    print('second_some:', second_instance.some_value, end='\n\n')

    # testing descriptor with argument
    instance.more_than_ten = 11
    try:
        instance.more_than_ten = 10
    except Exception as ex:
        print(f"We've got an exception: {ex}")
    print('greater_then_ten:', instance.more_than_ten, end='\n\n')

    # testing descriptor outside the class
    instance.different_value = OurDescriptor('different')
    print(instance.__dict__)
    instance.different_value = -2
    print('different:', instance.different_value)
    print(instance.__dict__)


test()
